# ECON 708 - Economía y Procesamiento de Datos No Tradicionales

Econ708 es un curso de introducción a la programación y al procesamiento de datos tradicionales y no tradicionales orientado a estudiantes de Economía en base al lenguaje [`R`](https://cran.r-project.org/). El curso tiene dos módulos. En la primera parte se desarrollan elementos de programación y manipulación de datos. La segunda parte cubre aplicaciones orientadas a la captura automática y procesamiento de datos, y al análisis de redes.

> Docentes: Sergio De Raco y Viktoriya Semeshenko

> Versión: Primer cuatrimestre 2024

```
15 de Marzo, 2024
```
  
## Objetivos de aprendizaje

* Entender los elementos y procedimientos básicos de la programación del lenguaje R.

* Comprender y poner en práctica el flujo de trabajo de un proyecto de datos: cargar, emprolijar, manipular, analizar y visualizar datos.

* Conocer diferentes formas de la representación gráfica de datos y comunicación de resultados.

* Familiarizarse con diferentes librerías del lenguaje de programación utilizado.

* Estimular la aplicación del abordaje computacional a problemas de interés económico y social de diversa índole.


## Requerimientos

- **Formales**: Matemática para Economistas (Código 288) y Estadística II (Código 285).

- **Deseables**: Interés por el trabajo participativo en equipo y el aprendizaje mediante prueba y error.

- **Técnicos**: Notebook personal con min 8Gb RAM y 10Gb de espacio en disco.


## Criterio de evaluación

Para la aprobación del curso se requiere de la elaboración y aprobación de un proyecto de datos final en grupos.


## Resumen de contenidos

* *Unidad 0. Instalación y setup de workplace para proyectos de datos en R*. Lenguaje R y entorno en RStudio. Interfase de línea de comandos (CLI) y RStudio.

* *Unidad 1a. Elementos de programación en R*. Introducción a elementos de programación (dato, conjunto de datos). Gestión de variables y almacenamiento. Estructuras de datos (tipos). Operaciones (pivoting/merges para dataframes). Sentencias condicionales y estructuras de control de flujos: `for`, `while`, `if`. Implementación de funciones ad-hoc. Análisis estadístico descriptivo básico. R con tidyverse. Piping. Gestión de flujos de trabajo en el ciclo de los proyectos de datos (PD): Importar. Preprocesar (Emprolijar, Transformar y Explorar). Visualizar. Modelizar. Comunicar.

* *Unidad 1b. Visualización y comunicación en R*. Nociones de graficación (forma, color, tamaño, color). Exploración y Visualización de datos. Introducción a ggplot, template. Casos: tidy datasets, Gapminder. Comunicación: Rmarkdown, Graphics, Notebooks, Presentaciones, Interactivos, Dashboards. 

* *Unidad 2a. Aplicaciones 1: Captura de datos no tradicionales*. Scraping 101 (rvest). CRAN vignette/ LADAL web crawling and scraping/Recursos para scraping en W3schools (tutoriales y selectores) 

* *Unidad 2b. Aplicaciones 2: Redes de interacción*. Introducción al análisis de redes en R. Statistical Analysis of Network Data. Tutorial Kateto. Casos: Redes Gutenberg/Redes sociales ONA Book (API tweets British network) 

* *Unidad 2c. Aplicaciones 3 (EXTRA, si alcanza el tiempo): Text mining*. Introducción al análisis de textos en R. Tidytext con novelas y datos de Twitter. 



## Fuentes consultadas

El material para el curso fue extraído y transformado de diversas fuentes:

-   [https://ladal.edu.au/webcrawling.html](https://ladal.edu.au/webcrawling.html)
-   [https://github.com/kolaczyk/sand](https://github.com/kolaczyk/sand)
-   [https://kateto.net/network-visualization](https://kateto.net/network-visualization)    
-   [https://paldhous.github.io/NICAR/2019/r-text-analysis.html](https://paldhous.github.io/NICAR/2019/r-text-analysis.html)
-   [https://ladal.edu.au/net.html](https://ladal.edu.au/net.html)
-   [https://github.com/keithmcnulty/ona_book](https://github.com/keithmcnulty/ona_book)
-   [https://psyteachr.github.io/hack-your-data/scrape-twitter.html](https://psyteachr.github.io/hack-your-data/scrape-twitter.html)
-   [https://juliasilge.github.io/tidytext/articles/topic_modeling.html](https://juliasilge.github.io/tidytext/articles/topic_modeling.html)


## Bibliografía básica 

+ [Wickham, H., & Grolemund, G. (2016). R for data science: import, tidy, transform, visualize, and model data. " O'Reilly Media, Inc.".](https://r4ds.had.co.nz/) en [español](https://es.r4ds.hadley.nz/index.html).
+ [R Core Development Team, (2000), _Introducción a R. Notas sobre R: Un entorno de programación para Análisis de Datos y Gráficos_.]( https://cran.r-project.org/doc/contrib/R-intro-1.1.0-espanol.1.pdf).

## Bibliografía ampliada

+ (A COMPLETAR)



# Entorno de trabajo

> Pasos para la instalación y configuración del software necesario (normalizado) para el curso. 

> IMPORTANTE: deben seguirse estos pasos independientemente de que tengan instalado el software (`R`, `RStudio`) en cualquiera de sus versiones.

Dado que el curso se construye sobre el aprendizaje y uso de un lenguaje de programación, está pensado para que **cada alumno trabaje individualmente en una máquina**, que *asumiremos* propia en función de que sólo es posible adquirir estas habilidades a través de la práctica por medio de (¡muchas!) pruebas y errores.

Para desarrollar los contenidos del curso utilizaremos el software estadístico [`R`](https://cran.r-project.org/) y [`RStudio Desktop`](https://www.rstudio.com/products/rstudio/) como el entorno de desarrollo integrado (IDE) para la gestión de proyectos de datos. Eventualmente usaremos blocs de notas [`Jupyter`](https://jupyter.org/) o [Google Colab](https://colab.research.google.com/) como una herramientas multilenguaje alternativas.

Adicionalmente, requeriremos instalar [`git`](https://git-scm.com/downloads), una herramienta para el control de versiones que servirá para gestionar de manera eficiente el desarrollo de los proyectos de datos que encararemos en el curso.

> **Requerimientos mínimos de hardware**:

> - Procesador 64 bits

> - 8Gb de RAM

> - Mínimo de 10Gb de espacio disponible

A continuación detallamos los pasos a seguir para poner a punto el entorno de trabajo.

## R y RStudio

1. Seguir instrucciones para instalar en https://posit.co/download/rstudio-desktop/

## Instalar git

1. Abrir una terminal y ejecutar el siguiente comando para chequear si ya está instalado:

``` bash 
git
```

2. Si lo anterior presenta un error, en Windows descargar e instalar [`git`](https://git-scm.com/downloads), en macOS o Linux usar el administrador de paquetes correspondiente.

> Nota: [Acá](https://docs.microsoft.com/es-mx/devops/develop/git/install-and-set-up-git) se puede consultar una guía provista por Microsoft

## Clonar repo(sitorio) del curso

Este repo del curso https://gitlab.com/netlab.iiep/econ-708 contendrá la información que compartiremos durante el curso y otras carpetas que utilizaremos en los proyectos y se actualizará a lo largo de la cursada. Para usarlo es necesario "clonarlo" con `git` en un directorio de trabajo que elijan ustedes.

1. Crear un directorio de trabajo (p.ej.: `<Usuario>/Documentos/Facultad/MI_CARPETA`) 

2. Abrir una `terminal` en esa posición.

3. Ejecutar

``` bash
git clone https://gitlab.com/netlab.iiep/econ-708
```

> Nota: Se creará la carpeta `econ-708` en el directorio de trabajo seleccionado.

El repositorio clonado tiene la siguiente estructura de directorios:

```
├── clases
├── codigo
├── data
├── otros-repos-interesantes
└── proyectos
    ├── codigo
    ├── data
    ├── output
    └── proyecto_final
```

Para la elaboración del proyecto final deberán crear su proyecto en la carpeta `proyectos/proyecto_final/GRUPO_NN` en base a las indicaciones que daremos oportunamente.
